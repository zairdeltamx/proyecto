<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use app\Htpp\Controllers\UserController;
use app\Htpp\Controllers\TbTemperaturaController;
use app\Htpp\Controllers\TbHumedadController;
use app\Htpp\Controllers\TbGasController;
use app\Htpp\Controllers\TbMovimientoController;
use app\Htpp\Controllers\TbTarjetaController;
use app\Htpp\Controllers\TbUltrasonicoController;
use app\Htpp\Controllers\TbLedController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  //  return $request->user();
//});

Route::post('register', 'App\Http\Controllers\UserController@register');
Route::post('login', 'App\Http\Controllers\UserController@authenticate');

Route::group(['middleware' => ['jwt.verify']], function() {

    Route::post('user','App\Http\Controllers\UserController@getAuthenticatedUser');
    Route::get('api','App\Http\Controllers\UserController@index');



});

    Route::get('Temperatura','App\Http\Controllers\TbTemperaturaController@index');
    Route::post('Temperatura',[TbTemperaturaController::class,'store']);
    Route::get('Temperatura/{id}',[TbTemperaturaController::class,'show']);
    Route::put('Temperatura/{id}',[TbTemperaturaController::class,'update']);
    Route::delete('Temperatura/{id}',[TbTemperaturaController::class,'destroy']);

    Route::get('Humedad','App\Http\Controllers\TbHumedadController@index');
    Route::post('Humedad',[TbHumedadController::class,'store']);
    Route::get('Humedad/{id}',[TbHumedadController::class,'show']);
    Route::put('Humedad/{id}',[TbHumedadController::class,'update']);
    Route::delete('Humedad/{id}',[TbHumedadController::class,'destroy']);
    
    Route::get('Gas','App\Http\Controllers\TbGasController@index');
    Route::post('Gas',[TbGasController::class,'store']);
    Route::get('Gas/{id}',[TbGasController::class,'show']);
    Route::put('Gas/{id}',[TbGasController::class,'update']);
    Route::delete('Gas/{id}',[TbGasController::class,'destroy']);

    Route::get('Movimiento','App\Http\Controllers\TbMovimientoController@index');
    Route::post('Movimiento',[TbMovimientoController::class,'store']);
    Route::get('Movimiento/{id}',[TbMovimientoController::class,'show']);
    Route::put('Movimiento/{id}',[TbMovimientoController::class,'update']);
    Route::delete('Movimiento/{id}',[TbMovimientoController::class,'destroy']);

    Route::get('Distanciaget','App\Http\Controllers\TbUltrasonicoController@index');
    Route::post('Distancia',[TbUltrasonicoController::class,'store']);
    Route::get('Distancia/{id}',[TbUltrasonicoController::class,'show']);
    Route::put('Distancia/{id}',[TbUltrasonicoController::class,'update']);
    Route::delete('Distancia/{id}',[TbUltrasonicoController::class,'destroy']);