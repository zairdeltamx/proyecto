<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tb_ultrasonico extends Model
{
    protected $table ="tb_ultrasonicos";
     
    protected $fillable = [
    
        'distancia',
        
    ];

    protected $primarykey='id';
    public function getJWTIdentifier()
    {
    	return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
    	return [];
    }
}
