<?php

namespace App\Http\Controllers;

use App\Models\Tb_ultrasonico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Illuminate\Support\Facades\Http;

class TbUltrasonicoController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $respuesta=HTTP::get('https://io.adafruit.com/api/v2/zairortiz99/feeds/distancia/data/last?X-AIO-Key=aio_AINw95mm8Sc1O2Y2ZXz8QyTCVZyf');
        $data=$respuesta->object();
            $model = new Tb_ultrasonico();
        
            $model->distancia=$data->value;
           
            $model->save();


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tb_ultrasonico::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'distancia guardado'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tb_ultrasonico $Tb_ultrasonico)
    {
        return response()->json([
            'res'=>true,
            'distancia'=>$Tb_ultrasonico
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tb_ultrasonico $Tb_ultrasonico)
    {
        $Tb_ultrasonico->update($Tb_ultrasonico->all());
        return response()->json([
            'res'=>true,
            'msg'=>'distancia actualizado'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tb_ultrasonico $Tb_ultrasonico)
    {
        $Tb_ultrasonico->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'distancia eliminado'
        ],200);
    }
}
